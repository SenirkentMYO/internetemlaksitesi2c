﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 1080px;
        }
        .auto-style10 {
            width: 887px;
            height: 725px;
        }
        .auto-style11 {
            height: 115px;
        }
        .auto-style12 {
            height: 725px;
            width: 400px;
        }
        .auto-style14 {
            width: 1230px;
            height: 288px;
        }
        .auto-style15 {
            width: 54%;
        }
        .auto-style16 {
            height: 50px;
        }
        .auto-style17 {
            height: 31px;
        }
        .auto-style18 {
            color: #FFFFFF;
        }
        .auto-style19 {
            width: 100%;
        }
        .auto-style20 {
            height: 96px;
        }
        </style>
</head>
<body style="height: 1085px" background="images/back.jpg">
    <form id="form1" runat="server">
        <table align="center" style="height: 798px" border="0">
            <tr>
                <td class="auto-style11" colspan="2">
                    <img alt="" class="auto-style14" src="images/banner.jpg" /></td>
            </tr>
      
            <tr>
                 <td class="auto-style12" align="left">
                     <table class="auto-style15" align="left">
                         <tr>
                             <td class="auto-style16">
                                 <asp:Button ID="btndefault" runat="server" Height="50px" Text="ANASAYFA" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" style="font-weight: 700" />
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <asp:Button ID="btnev" runat="server" Height="50px" Text="EV" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" OnClick="btnev_Click" style="font-weight: 700" />
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <asp:Button ID="btnisyeri" runat="server" Height="50px" Text="İŞYERİ &amp; DÜKKAN" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" OnClick="btnisyeri_Click" style="font-weight: 700" />
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <asp:Button ID="btnarsa" runat="server" Height="50px" Text="ARSA" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" OnClick="btnarsa_Click" style="font-weight: 700" />
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <asp:Button ID="btnproje" runat="server" Height="50px" Text="PROJELERİMİZ" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" OnClick="btnproje_Click" style="font-weight: 700" />
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <asp:Button ID="btnuyeol" runat="server" Height="50px" Text="ÜYE OL" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" OnClick="btnuyeol_Click" style="font-weight: 700" />
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <asp:Button ID="btnuyegirisi" runat="server" Height="50px" Text="ÜYE GİRİŞİ" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" OnClick="btnuyegirisi_Click" style="font-weight: 700" />
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <asp:Button ID="btnhakimizda" runat="server" Height="50px" Text="HAKKIMIZDA" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" OnClick="btnhakimizda_Click" style="font-weight: 700" />
                             </td>
                         </tr>
                         <tr>
                             <td>
                                 <asp:Button ID="btnbizeulasin" runat="server" Height="50px" Text="BİZE ULAŞIN" Width="210px" BackColor="#669900" BorderColor="#669900" ForeColor="White" OnClick="btnbizeulasin_Click" style="font-weight: 700" />
                             </td>
                         </tr>
                     </table>
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />

                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                 </td>
                 <td class="auto-style10">
                     <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="White" style="font-weight: 700; font-size: xx-large" Width="606px" OnClick="LinkButton1_Click">Binanızda Isı Yatılım Yoksa Dikkat Edin !</asp:LinkButton>
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <table class="auto-style19">
                         <tr>
                             <td class="auto-style20"><span class="auto-style18">Bu yönetmelik, binalardaki ısı kayıplarının azaltılması, enerji tasarrufu sağlanması ve uygulama esaslarının belirlenmesi amacıyla hazırlanmıştır.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bu yönetmelik, 3030 sayılı Büyük Şehir Belediyelerinin Yönetimi Hakkında Kanun Hükmünde kararnamenin Değiştirilerek Kabulü Hakkında Kanun kapsamındaki belediyeler dahil, bütün yerleşim birimlerindeki binalarda uygulanır.
                                     <br />
                                     Münferit olarak inşa edilen ve ısıtılmasına gerek duyulmayan depo, cephanelik, ardiye, ahır, ağıl ve benzeri binalarda bu yönetmelik hükümleri uygulanmaz. 180 sayılı Bayındırlık ve İskan Bakanlığının Teşkilat ve Görevleri Hakkında Kanun Hükmünde Kararnamenin 209 sayılı Kanun Hükmünde Kararname ile değişik 32 nci maddesi kapsamına giren kamu kurum ve kuruluşları, katma bütçeli idareler, il özel idareleri ve belediyeler bu yönetmeliğe uymak ve uygulamakla yükümlüdürler.<br />
                                     <br />
                                     <br />
                                     Yurdumuz, binalarda ısı yalıtımı uygulamaları bakımından dört ısı bölgesine ayrılmış ve bu bölgelere giren il ve ilçeler (EK: 1-A)&#39;daki listede ve (EK: 1-B)&#39;de harita üzerinde dört grupta gösterilmiştir. Listede yer almayan belediyeler, bağlı oldukları ilçe değerlerini esas alacaklardır. Birinci bölgede yapılacak binalarda, merkezi klima sistemi uygulanacak ise bu binalarda yapılacak ısı yalıtım projesinde ikinci bölge için verilmiş olan sınır değerler geçerli olacaktır.<br />
                                     <br />
                                     <br />
                                     Binalar, ısı kayıpları bakımından çevre şart ve gereklerine uygun düzeyde yalıtılacaktır. Binaların hesaplanan yıllık ısıtma enerjisi ihtiyacı, (Tablo 1)&#39;de bölgelere göre verilen yıllık ısıtma enerjisi değerlerini aşmamalıdır.<br />
                                     <br />
                                     Hesaplamalarda kullanılacak çeşitli yapı malzeme ve bileşenlerinin ısıl iletkenlik hesap değerleri ile iç ve dış yüzeysel ısıl iletkenlik direnç değerleri TS 825&#39;den, hava tabakalarının ısıl geçirgenlik dirençleri ile pencere ve dış kapıların ısıl geçirgenlik katsayıları TS 2164&#39;den alınacaktır.</span></td>
                             </tr>
                         </table>
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                     <br />
                 </td>
            </tr>
          <tr>
               <td class="auto-style17" colspan="2" align="center" ><strong><span class="auto-style18">Copyright © Kuby ve Ortaklarıt © Kuby ve Ortakları</span></strong></td>
            </tr>
        </table>
    
    </form>
</body>
</html>
