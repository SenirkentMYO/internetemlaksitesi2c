﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnev_Click(object sender, EventArgs e)
    {
        Response.Redirect("ev.aspx");
    }
    protected void btnisyeri_Click(object sender, EventArgs e)
    {
        Response.Redirect("isyeri.aspx");
    }
    protected void btnarsa_Click(object sender, EventArgs e)
    {
        Response.Redirect("arsa.aspx");
    }
    protected void btnproje_Click(object sender, EventArgs e)
    {
        Response.Redirect("proje.aspx");
    }
    protected void btnuyeol_Click(object sender, EventArgs e)
    {
        Response.Redirect("uyeol.aspx");
    }
    protected void btnuyegirisi_Click(object sender, EventArgs e)
    {
        Response.Redirect("uyegirisi.aspx");
    }
    protected void btnhakimizda_Click(object sender, EventArgs e)
    {
        Response.Redirect("hakkimizda.aspx");
    }
    protected void btnbizeulasin_Click(object sender, EventArgs e)
    {
        Response.Redirect("iletisim.aspx");
    }
    protected void btndefault_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}